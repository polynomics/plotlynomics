# plotlynomics #

Das R-Package `plotlynomics` beinhaltet alle Standardabbildungen von Polynomics
im plotly Format. Solche Abbildungen können dann sowohl in Berichten als auch
online im Polynomics BI-Portal verwendet werden. plotly macht es möglich im
Webformat dynamische Elemente in Abbildungen zu verwenden.

Die Erstellung von statischen Abbildungen setzt die lokale Installation von Python voraus.

Die Sammlung an Abbildung wird stetig erweitert. Bei Fragen oder Feedback:
@TSA oder @TVR

### Installation ###

Das Package kann direkt in der RStudio-Konsole über Bitbucket geladen werden.
Führe dazu folgende Befehle aus:


Da das Repository vorübergehend auf öffentlich gestellt ist, müssen für die Installation **keine**
Zugangsdaten (Benutzername & Passwort) zu eurem Bitbucket eingegeben werden.

```R

library(remotes)
install_bitbucket("polynomics/plotlynomics")

library(plotlynomics)

```

Sobald das Package einmal installiert ist, muss es später nur noch durch den
Befehl `library(plotlynomics)` geladen werden. Im Falle einer neuen Package-Version
muss jedoch das gesamte Package neu installiert werden.

### Zusätzliche Infos ###

Auf unserem Wiki finden sich noch mehr Informationen zur Entwicklung und
Verwaltung unserer plotly-Abbildungen: TBD
