# ------------------ #
# Tests für poly_bar #
# ------------------ #


# Test 1: eine-yvar -----
test_name <- "eine-yvar"

test_that(test_name, {
  p <- poly_stack(data_bar,
                  over = "rep78",
                  yvars = c("weight"),
                  xtitle = "axis-title",
                  ytitle = "axis-title",
                  plottitle = "Title")

  expect_doppelganger_plotly(name = test_name, p = p)
}
)

# Test 2: mehrere-yvar -----
test_name <- "mehrere-yvar"

test_that(test_name, {
  p <- poly_stack(data_bar,
                  over = "rep78",
                  yvars = c("weight", "length", "price"),
                  xtitle = "axis-title",
                  ytitle = "axis-title",
                  plottitle = "Title")

  expect_doppelganger_plotly(name = test_name, p = p)
}
)

# Test 3: mit-x-label -----
test_name <- "mit-x-label"

test_that(test_name, {
  p <- poly_stack(data_bar,
                  over = "rep78",
                  yvars = c("weight", "length", "price"),
                  xtitle = "axis-title",
                  ytitle = "axis-title",
                  plottitle = "Title",
                  show_x_label = TRUE)

  expect_doppelganger_plotly(name = test_name, p = p)
}
)

# Test 4: mit-medline-meanline-label -----
test_name <- "mit-medline-meanline-label"

test_that(test_name, {
  p <- poly_stack(data_bar,
                  over = "rep78",
                  yvars = c("weight", "length", "price"),
                  xtitle = "axis-title",
                  ytitle = "axis-title",
                  plottitle = "Title",
                  show_x_label = TRUE,
                  medline = TRUE,
                  meanline = TRUE)

  expect_doppelganger_plotly(name = test_name, p = p)
}
)

# Test 5: mit-outlier -----
test_name <- "mit-outlier"

test_that(test_name, {
  p <- poly_stack(data_bar,
                  over = "rep78",
                  yvars = c("weight", "length", "price"),
                  outlier = "mit",
                  xtitle = "axis-title",
                  ytitle = "axis-title",
                  plottitle = "Title",
                  show_x_label = TRUE)

  expect_doppelganger_plotly(name = test_name, p = p)
}
)

# Test 6: copy-size -----
test_name <- "copy-size"

test_that(test_name, {
  p <- poly_stack(data_bar,
                  over = "rep78",
                  yvars = c("weight", "length", "price"),
                  xtitle = "axis-title",
                  ytitle = "axis-title",
                  plottitle = "Title",
                  copy_size  = 8,
                  show_x_label = TRUE)

  expect_doppelganger_plotly(name = test_name, p = p)
}
)

# Test 7: no note -----
test_name <- "no-note"

test_that(test_name, {
  p <- poly_stack(data_bar,
                  over = "rep78",
                  yvars = c("weight", "length", "price"),
                  xtitle = "axis-title",
                  ytitle = "axis-title",
                  plottitle = "Title",
                  nonote  = TRUE,
                  show_x_label = TRUE)

  expect_doppelganger_plotly(name = test_name, p = p)
}
)

# Test 8: no stack -----
test_name <- "no-stack"

test_that(test_name, {
  p <- poly_stack(data_bar,
                  over = "foreign",
                  yvars = c("weight", "length", "price"),
                  xtitle = "axis-title",
                  ytitle = "axis-title",
                  plottitle = "Title",
                  no_stack = TRUE,
                  show_x_label = TRUE)

  expect_doppelganger_plotly(name = test_name, p = p)
}
)


# Test 9: no copy right ----
test_name <- "no_copy_right"

test_that(test_name, {
  p <- poly_stack(data_bar,
                  over = "foreign",
                  yvars = c("weight", "length", "price"),
                  xtitle = "axis-title",
                  ytitle = "axis-title",
                  plottitle = "Title",
                  hoverinfo = TRUE,
                  show_copy_right = FALSE,
                  show_x_label = TRUE)

  expect_doppelganger_plotly(name = test_name, p = p)
}
)


# Test 10: order-by ----
test_name <- "order_by"

test_that(test_name, {
  p <- poly_stack(data_bar,
                  over = "id",
                  yvars = c("weight", "price", "length"),
                  xtitle = "axis-title",
                  ytitle = "axis-title",
                  plottitle = "Title",
                  outlier = "ohne",
                  meannote = TRUE,
                  meanline = TRUE,
                  medline = TRUE,
                  hoverinfo = TRUE,
                  nr_format = "ch",
                  order_by = "weight"
  )


  expect_doppelganger_plotly(name = test_name, p = p)
}
)


# Test 11: horizontal ----
test_name <- "horizontal"

test_that(test_name, {
  p <- poly_stack(data_bar,
                  over = "id",
                  yvars = c("price", "weight", "length"),
                  xtitle = "axis-title",
                  ytitle = "axis-title",
                  plottitle = "Title",
                  meannote = TRUE,
                  hoverinfo = TRUE,
                  meanline = TRUE,
                  medline = TRUE,
                  horizontal = TRUE,
                  ind = c(4, 55, 61)
  )



  expect_doppelganger_plotly(name = test_name, p = p)
}
)


# Test 12: manuelle notiz ----
test_name <- "man_note"

test_that(test_name, {
  p <- poly_stack(data_bar,
                  over = "id",
                  yvars = c("price", "weight", "length"),
                  xtitle = "axis-title",
                  ytitle = "axis-title",
                  plottitle = "Title",
                  add_man_note = "testnotiz",
                  ind = c(4, 55, 61)
  )



  expect_doppelganger_plotly(name = test_name, p = p)
}
)


# Test 13: man_group_col ----
test_name <- "man_group_col"

test_that(test_name, {
  man_group_col <- c("black", "yellow", "green")

  p <- poly_stack(data_bar,
                  over = "id",
                  yvars = c("price", "weight", "length"),
                  xtitle = "axis-title",
                  ytitle = "axis-title",
                  man_group_col = man_group_col
  )



  expect_doppelganger_plotly(name = test_name, p = p)
}
)

# Test 14: multiple ind and order ----
test_name <- "multiple_ind_order"

test_that(test_name, {
  man_group_col <- c("black", "yellow", "green")

  p <- poly_stack(data_bar,
                  over = "id",
                  ind = c(2, 22),
                  outlier = "ohne",
                  yvars = c("price", "weight", "length"),
                  xtitle = "axis-title",
                  ytitle = "axis-title",
                  plottitle = "Title",
                  hoverinfo = TRUE,
                  show_x_label = TRUE,
                  horizontal = FALSE,
                  order_by = "price"
  )



  expect_doppelganger_plotly(name = test_name, p = p)
}
)

# Test 15: clean note ----
test_name <- "clean_note"

test_that(test_name, {

  p <- poly_stack(data_bar,
                  over = "headroom",
                  yvars = c("price", "weight", "length"),
                  xtitle = "axis-title",
                  ytitle = "axis-title",
                  plottitle = "Title",
                  medline = FALSE,
                  mednote = FALSE,
                  meannote = FALSE,
                  meanline = FALSE
  )



  expect_doppelganger_plotly(name = test_name, p = p)
}
)


# Test 16: big legend ----
test_name <- "big_legend"

test_that(test_name, {

  temp <- data_bar %>%
    mutate(price_1 = price * 1.1,
           price_2 = price * 1.1,
           price_3 = price * 1.1,
           price_4 = price * 1.1,
           price_5 = price * 1.1,
           price_6 = price * 1.1,
           price_7 = price * 1.1,
           price_8 = price * 1.1,
           price_9 = price * 1.1,
           price_10 = price * 1.1,
           price_11 = price * 1.1,
           price_12 = price * 1.1,
    )

  p <- poly_stack(temp,
                  over = "headroom",
                  yvars = c("price",
                            "price_1",
                            "price_2",
                            "price_3",
                            "price_4",
                            "price_5",
                            "price_6",
                            "price_7",
                            "price_8",
                            "price_9",
                            "price_10",
                            "price_11",
                            "price_12"),
                  xtitle = "axis-title",
                  ytitle = "axis-title",
                  plottitle = "Title",
                  medline = FALSE,
                  mednote = FALSE,
                  meannote = TRUE,
                  meanline = FALSE
  )



  expect_doppelganger_plotly(name = test_name, p = p)
}
)



# Test 17: hover anonym ----
test_name <- "hover_anonym"

test_that(test_name, {

  p <- poly_stack(data_bar,
                  over = "id",
                  outlier = "ohne",
                  yvars = c("price", "weight", "length"),
                  xtitle = "",
                  ytitle = "axis-title",
                  plottitle = "Title",
                  hoverinfo = TRUE,
                  hover_anonym = TRUE,
  )



  expect_doppelganger_plotly(name = test_name, p = p)
}
)

# Test 18: neg Werte ----
test_name <- "neg_values"

test_that(test_name, {
  temp <- data_bar %>%
    mutate(weight = (-1)*weight)

  p <- poly_stack(temp,
                  over = "id",
                  outlier = "ohne",
                  yvars = c("price", "weight", "length"),
                  xtitle = "",
                  ytitle = "axis-title",
                  plottitle = "Title",
                  hoverinfo = TRUE
  )



  expect_doppelganger_plotly(name = test_name, p = p)
}
)

# Test 18: anzahl digits ----
test_name <- "man_nr_digits"

test_that(test_name, {

  p <- poly_stack(data_bar,
                  over = "id",
                  outlier = "ohne",
                  yvars = c("price", "weight", "length"),
                  xtitle = "",
                  ytitle = "axis-title",
                  plottitle = "Title",
                  hoverinfo = TRUE,
                  man_nr_digits = 3
  )



  expect_doppelganger_plotly(name = test_name, p = p)
}
)

# Test 20: Missings bei ind ----
test_name <- "missing_yvar_ind"


test_that(test_name, {

  p <- poly_stack(data_bar,
             over = "id",
             ind = c(15, 6),
             outlier = "ohne",
             yvars = c("price")
  )




  expect_doppelganger_plotly(name = test_name, p = p)
}
)

# Test 21: percentage ----
test_name <- "percentage"


test_that(test_name, {

  p <- poly_stack(data_bar,
                  over = "id",
                  ind = c(6, 33),
                  outlier = "ohne",
                  yvars = c("price", "length"),
                  percentage = TRUE,
                  hoverinfo = TRUE,
                  order_by = "length",
                  man_nr_digits = 1
  )



  expect_doppelganger_plotly(name = test_name, p = p)
}
)

# Test 22: percentage nostack----
test_name <- "percentage_nostack"


test_that(test_name, {

  p <- poly_stack(data_bar,
                  over = "rep78",
                  outlier = "ohne",
                  yvars = c("price", "length"),
                  percentage = TRUE,
                  hoverinfo = TRUE,
                  order_by = "price",
                  no_stack = TRUE
  )



  expect_doppelganger_plotly(name = test_name, p = p)
}
)

# Test 23:  font_size & font_type ----
test_name <- "font_size_type"


test_that(test_name, {

  p <- poly_stack(data_bar,
                  over = "rep78",
                  outlier = "ohne",
                  yvars = c("price", "length"),
                  percentage = TRUE,
                  hoverinfo = TRUE,
                  order_by = "price",
                  no_stack = TRUE,
                  font_type = "Calibri Light",
                  font_size = 8,
                  leg_size = 20
  )



  expect_doppelganger_plotly(name = test_name, p = p)
}
)

# Test 24:  long note ----
test_name <- "long_note"


test_that(test_name, {

  p <- poly_stack(data_bar,
                  xtitle = "axis title",
                  show_x_label = TRUE,
                  ytitle = "axis title",
                  over = "id",
                  outlier = "ohne",
                  yvars = c("price", "length"),
                  meannote = TRUE,
                  add_man_note = "Zusatzzeile 1<br>Zusatzzeile 2",
                  font_type = "Calibri",
                  font_size = 16
  )



  expect_doppelganger_plotly(name = test_name, p = p)
}
)

# Test 25:  suffix ----
test_name <- "y_suffix"


test_that(test_name, {

  p <- poly_stack(data_bar,
                  xtitle = "axis title",
                  show_x_label = TRUE,
                  ytitle = "axis title",
                  over = "id",
                  outlier = "ohne",
                  yvars = c("price", "length"),
                  y_suffix = " CHF",
                  meannote = TRUE,
                  add_man_note = "Zusatzzeile 1<br>Zusatzzeile 2",
                  font_type = "Calibri",
                  font_size = 16
  )



  expect_doppelganger_plotly(name = test_name, p = p)
}
)


# Test 26:  Translation ----
test_name <- "translation"


test_that(test_name, {

  p <- poly_stack(data_bar,
                  over = "id",
                  show_x_label = FALSE,
                  outlier = "ohne",
                  yvars = c("price", "length"),
                  y_suffix = " CHF",
                  meannote = TRUE,
                  hoverinfo = TRUE,
                  add_man_note  = "TEST: Dieser Text bleibt deutsch.",
                  ind = 19,
                  lang = "fr"
  )



  expect_doppelganger_plotly(name = test_name, p = p)
}
)
